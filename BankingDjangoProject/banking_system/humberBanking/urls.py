from django.urls import path
from humberBanking.views import index, register, my_login, dashboard,deposit,withdraw, transaction_history,logout_view

urlpatterns = [
    path('', index, name ='Home'),
    path('register/', register, name='register'),
    path('login/', my_login, name='login'),
    path('dashboard/', dashboard, name='dashboard'),
    path('deposit/', deposit, name='deposit'),
    path('withdraw/', withdraw, name='withdraw'),
    path('transaction_history/', transaction_history, name='transaction_history'),
    path('register/', register, name='register'),
    path('logout/', logout_view, name='logout'),

]