from decimal import Decimal
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from humberBanking.models import Account, Transaction, Registration,Deposit
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User

def index(request):
    return render(request, 'index.html')


def register(request):
    if request.method == 'POST':
        registration = Registration(
            first_name=request.POST['first_name'],
            last_name=request.POST['last_name'],
            email=request.POST['email'],
            account_type=request.POST['account_type'],
            gender=request.POST['gender'],
            date_of_birth=request.POST['date_of_birth'],
            password=request.POST['password'],
            address=request.POST['address'],
            city=request.POST['city'],
            country=request.POST['country'],
            postal_code=request.POST['postal_code'],
        )
        registration.save()
        act = Account(name=request.POST['first_name'],email = request.POST['email'],balance=0.0)
        act.save()
        user = User.objects.create_user(username=registration.email,
                                        email=registration.email,password=registration.password)
        user.is_active = True
        user.save()
        return redirect('login')
    else:
        return render(request, 'register.html')

def my_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            email = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=email, password=password)
            if user is not None:
                login(request, user)
                return redirect('dashboard')
            else:
                form.add_error(None, 'Invalid email or password')
        else:
            form.add_error(None, 'Form is not valid')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

@login_required(login_url='/login/')
def dashboard(request):
    account = Account.objects.get(email=request.user.email)
    trans = Transaction(account=account,transaction_type='D', amount= account.balance,account_id =1)
    transactions = Transaction.objects.filter(account=account)
    trans.save()
    context = {'balance' : account.balance, 'name': account.name}
    return render(request, 'dashboard.html',context)

def deposit(request):
    if request.method == 'POST':
        amount = request.POST.get('amount')
        account = Account.objects.get(email=request.user.email)
        account.balance += Decimal(amount)
        account.save()
        transaction = Transaction(account=account, transaction_type='D', amount=amount)
        transaction.save()
        success_msg = "Deposit successful!!"
        return redirect('/dashboard/?success_message='+ success_msg)
    else:
        accounts = Account.objects.filter(email=request.user.email)
        return render(request, 'accounts/deposit.html', {'accounts': accounts})
    
def withdraw(request):
    if request.method == 'POST':
        amount = request.POST.get('amount')
        account = Account.objects.get(email=request.user.email)

        # Check if withdrawal amount is greater than account balance
        if((account.balance - Decimal(amount))<0):
            error_msg = "You don't have enough amount to withdraw"
            return redirect('/withdraw/?error_message=' + error_msg)

        else:
            account.balance -= Decimal(amount)
            account.save()
            transaction = Transaction(account=account, transaction_type='W', amount=amount)
            transaction.save()
            success_msg = "Withdrawal successful!!"
            return redirect('/dashboard/?success_message='+ success_msg)
    else:
        accounts = Account.objects.filter(email=request.user.email)
        return render(request, 'accounts/withdraw.html', {'accounts': accounts})
    

def transaction_history(request):
    account = Account.objects.get(email=request.user.email)
    transactions = Transaction.objects.filter(account=account)
    transactions2 = Transaction.objects.filter(transaction_type=request.POST.get('transaction_type'))
    transactions3 = Transaction.objects.filter(amount=request.POST.get('amount'))

    return render(request, 'accounts/transaction_history.html', {'transactions': transactions,'transaction.type':transactions2, 'transactions.amount':transactions3})

def logout_view(request):
    logout(request)
    return redirect('login')